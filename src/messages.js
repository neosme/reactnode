export default {
    en : {
        "nav.id": "Id",
        "nav.name": "Name",
        "nav.description": "Description",
        "nav.long description": "Long Description",
        "nav.completed": "Completed",
        "nav.view": "View",
        "nav.tableName": "Employee list",
        "nav.noData": "No data found"
    },
    fr : {
        "nav.id": "Identifiant",
        "nav.name": "Nom",
        "nav.description": "La Description",
        "nav.long description": "Longue Description",
        "nav.completed": "Terminé",
        "nav.view": "Vue",
        "nav.tableName": "Liste des employés",
        "nav.noData": "Aucune donnée disponible."
    }
}