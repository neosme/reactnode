import React, { Component } from 'react';
import { Table, Divider, Tag, Card, Col, Row, Button, Icon, Modal } from 'antd';
import 'antd/dist/antd.css';
import { withTranslation, Trans } from 'react-i18next';
import {FormattedMessage} from 'react-intl'

export class TableView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            tableViewModalVisible: false,
            modalData: {}
        }
    };

    componentDidMount() {
        this.callAPI();
    }

    onEditRow = (text, record, key) => {
        console.log('text, record, key', text, record, key)
        this.setState({
            modalData: record
        }, () =>{
            this.setState({
                tableViewModalVisible: true
            })
        })
    }

    handleCancel = () => {
        console.log('im in cancel module')
        this.setState({
            tableViewModalVisible: false
        })
    }

    callAPI = () => {
         fetch("http://172.104.179.88:9000/testAPI")
            .then((response) => response.json())
            .then((responseJSON) => {
                // do stuff with responseJSON here...
                console.log('responseJSON', responseJSON);
                this.setState({
                    data: responseJSON
                }, () => {
                    console.log("this.state.data", this.state.data)
                })
            });
    }

    languageFunction = (val) => {
        const { i18n } = this.props.taranslate;
        if(val === 'english'){
            i18n.changeLanguage('en')
        }
        else{
            i18n.changeLanguage('fr')
        }
    }

    render() {

        const { t, i18n } = this.props.taranslate;

        console.log('dasldkhkjh123123', this.props.taranslate)
        const {tableViewModalVisible, modalData, data} = this.state;

        const columns = [
            {
                title: t('welcome.id'),
                dataIndex: 'Id',
                key: 'id',
            },
            {
                title: t('welcome.name'),
                dataIndex: 'Name',
                key: 'name',
            },
            {
                title: t('welcome.description'),
                dataIndex: 'Description',
                key: 'description',
            },
            {
                title: t('welcome.long description'),
                dataIndex: 'LongDescription',
                key: 'description',
            },
            {
                title: t('welcome.completed'),
                dataIndex: 'Complete',
                key: 'isCompleted',
                render: (text, record, key) => {
                    return <div>
                        {text === 1 ? <p>true</p> : <p>false</p>}
                    </div>
                }
            },
            {
                title: t('welcome.view'),
                dataIndex: 'operation',
                width: 50,
                key: 'edit',
                className: 'scrollableColumn',
                render: (text, record, key) => {
                    return <Icon type="eye" onClick={() => this.onEditRow(text, record, key)}/>
                }
            }
        ];

        return (
            <Card className="onBoardingScreen">
                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', margin: '10px'}}>
                    <h1>{ t('welcome.tableName') }</h1>
                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <div style={{margin: '10px'}}>
                            <Button onClick={()=>this.languageFunction('english')}>English</Button>
                        </div>
                        <div style={{margin: '10px'}}>
                            <Button onClick={()=>this.languageFunction('other')}>French</Button>
                        </div>
                    </div>
                </div>
                <div className="onBoardingScreen">
                <Table
                    pagination={false}
                    dataSource={data}
                    columns={columns}
                    bordered={true}
                    key={'table'}
                    locale={{ emptyText: t('welcome.noData') }}
                />
                    <Modal
                        title="Detailed View"
                        visible={tableViewModalVisible}
                        onCancel={this.handleCancel}
                        footer={null}
                    >
                        <p><b>ID:</b> {modalData.Id}</p>
                        <p><b>Name:</b> {modalData.Name}</p>
                        <p><b>Description:</b> {modalData.Description}</p>
                        <p><b>Long Description:</b> {modalData.LongDescription}</p>
                        <p><b>Completed:</b> {modalData.Complete === 1 ? true : false}</p>
                    </Modal>
                </div>
            </Card>
        );
    }
}

export default withTranslation('common')(TableView);