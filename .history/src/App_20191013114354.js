import React from 'react';
import logo from './logo.svg';
import './App.css';
import {TableView} from "./components/table/tableView";
import { Table, Divider, Tag, Card, Col, Row, Button } from 'antd';
import { withTranslation, Trans } from 'react-i18next';
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import reducers from './reducers/index'


function App(props) {
  const createStoreWithMiddleware = applyMiddleware(thunk)(createStore)
  console.log('dasldjlkdj123123', props)
  return (
    <Provider store={createStoreWithMiddleware(reducers)}>
      <TableView  taranslate={props}/>
    </Provider>
  );
}

export default withTranslation('common')(App);