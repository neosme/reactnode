/** @format */

import {
  GET_BOOKING_HISTORY,
  GET_BOOKING_HISTORY_SUCCESS,
  GET_BOOKING_HISTORY_FAIL,
  GET_BOOKING_DETAILS,
  GET_BOOKING_SUCCESS_DETAILS,
  GET_BOOKING_FAIL_DETAILS,
} from './types.js'


export const getBookingList = (client, offset, startTime, endTime) => async dispatch => {
  dispatch({type: GET_BOOKING_HISTORY})
 
}

export const getBookingDetails = (client, bookingId) => async dispatch => {
  dispatch({type: GET_BOOKING_DETAILS})
}
