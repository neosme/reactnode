/** @format */

export const SET_LOCAL = 'set_local'
export const SET_LOCAL_SUCCESS = 'set_local_success'
export const SET_LOCAL_FAIL = 'set_local_fail'

export const GET_BOOKING_DETAILS = 'get_booking_details'
export const GET_BOOKING_SUCCESS_DETAILS = 'get_booking_success_details'
export const GET_BOOKING_FAIL_DETAILS = 'get_booking_fail_details'

export const GET_BOOKING_HISTORY = 'get_booking_history'
export const GET_BOOKING_HISTORY_SUCCESS = 'get_booking_history_success'
export const GET_BOOKING_HISTORY_FAIL = 'get_booking_history_fail'