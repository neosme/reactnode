/** @format */

import {
  GET_BOOKING_HISTORY,
  GET_BOOKING_HISTORY_SUCCESS,
  GET_BOOKING_HISTORY_FAIL,
  GET_BOOKING_DETAILS,
  GET_BOOKING_SUCCESS_DETAILS,
  GET_BOOKING_FAIL_DETAILS,
} from './types.js'
import gql from 'graphql-tag'
import * as gqlTag from '../common/gql'

export const getBookingList = (client, offset, startTime, endTime) => async dispatch => {
  dispatch({type: GET_BOOKING_HISTORY})
  try {
    const gqlValue = gqlTag.query.booking.listWithFiltersGQLTAG({
      pFromTime: startTime,
      pToTime: endTime,
      first: offset,
      offset: 0,
    })
    const query = gql`
      ${gqlValue}
    `
    const {data} = await client.query({
      query,
    })
    const {nodes} = data.listBookingByDateRange

    if (nodes && nodes.length > 0) {
      return dispatch({type: GET_BOOKING_HISTORY_SUCCESS, payload: nodes})
    } else {
      return dispatch({type: GET_BOOKING_HISTORY_FAIL, payload: []})
    }
  } catch (err) {
    return dispatch({type: GET_BOOKING_HISTORY_FAIL, payload: err.message})
  }
}

export const getBookingDetails = (client, bookingId) => async dispatch => {
  dispatch({type: GET_BOOKING_DETAILS})
  try {
    const gqlValue = gqlTag.query.booking.byIdGQLTAG
    const query = gql`
      ${gqlValue}
    `
    const {data} = await client.query({
      query,
      variables: {
        chefBookingHistId: bookingId,
      },
    })
    if (data.chefBookingHistoryByChefBookingHistId) {
      return dispatch({
        type: GET_BOOKING_SUCCESS_DETAILS,
        payload: data.chefBookingHistoryByChefBookingHistId,
      })
    } else {
      return dispatch({type: GET_BOOKING_FAIL_DETAILS, payload: {}})
    }
  } catch (err) {
    return dispatch({type: GET_BOOKING_FAIL_DETAILS, payload: err.message})
  }
}
