export default {
    en : {
        "nav.id": "Id",
        "nav.name": "Name",
        "nav.description": "Description",
        "nav.long description": "Long Description",
        "nav.completed": "Completed",
        "nav.view": "View",
        "nav.tableName": "Employee list",
        "nav.noData": "No data found"
    },
    fr : {
        
    }
}