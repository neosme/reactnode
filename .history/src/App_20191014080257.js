import React from 'react';
import logo from './logo.svg';
import './App.css';
import { IntlProvider } from 'react-intl'
import {TableView} from "./components/table/tableView";
import { Table, Divider, Tag, Card, Col, Row, Button } from 'antd';
import { withTranslation, Trans } from 'react-i18next';
import {messages} from './messages'


function App(props) {
  console.log('dasldjlkdj123123', props.i18n.language)
  return (
    <IntlProvider locale={props.i18n.language} messages={messages[props.i18n.language]}>
      <TableView  taranslate={props}/>
    </IntlProvider>
  );
}

export default withTranslation('common')(App);