/** @format */

import {
    SET_LOCAL
  } from '../actions/types.js'
  
  const INITIAL_STATE = {
    lang: 'en',
  }
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case SET_LOCAL:
        return {...state, lang: action.payload}
      default:
        return state
    }
  }
  