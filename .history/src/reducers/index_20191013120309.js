import {combineReducers} from 'redux'
import LocalReducer from './localReducer'


const rootReducer = combineReducers({
  LocalData: LocalReducer,
})

export default rootReducer